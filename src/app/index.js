import models from './models';
import services from './services';

import server from './server';


const app = {
    async init() {
        await models.init();
        await services.init();
        await server.init();
    },

};

app.init().then(() => {
    console.log('APP started...');
});
