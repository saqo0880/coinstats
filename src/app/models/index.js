import MongoModel from './mongo';
import RedisModel from './redis';

export default {
    init() {
        return Promise.all([
            MongoModel.connect(),
            RedisModel.connect(),
        ]);
    },
};
