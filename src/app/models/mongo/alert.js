export default mongoose => {
    return mongoose.model(
        "alerts",
        mongoose.Schema(
            {
                symbol: {
                    type: String,
                    // unique: true,
                    required: true
                },
                min: {
                    type: Number,
                    required: true
                },
                max: {
                    type: Number,
                    required: true
                },
                userId: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'users'
                },
                isActive: {
                    type: Boolean,
                    default: true
                }
            },
            {timestamps: true}
        )
    );
};
