import mongoose from 'mongoose';
import users from "./user";
import alerts from "./alert";

mongoose.Promise = global.Promise;
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);


export default {
    connect() {
        const db = {};
        db.url = `mongodb+srv://${CONFIGS.MONGO.user}:${CONFIGS.MONGO.password}@cluster0.2r11q.mongodb.net/${CONFIGS.MONGO.database}?retryWrites=true&w=majority`
        db.users = users(mongoose)
        db.alerts = alerts(mongoose)
        global.DB = db;

        // db.users.fin

        return new Promise((res, rej) => {
            mongoose
                .connect(DB.url, {
                    useNewUrlParser: true,
                    useUnifiedTopology: true
                })
                .then(() => {
                    console.log("Connected to the database!")
                    res();
                })
                .catch(err => {
                    console.log(err);
                    rej("Cannot connect to the database!")
                });
        })
    },
};
