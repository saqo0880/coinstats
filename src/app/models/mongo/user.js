export default mongoose => {
    return mongoose.model(
        "users",
        mongoose.Schema(
            {
                key: {
                    type: String,
                    unique: true,
                    required: true
                },
                secret: {
                    type: String,
                    unique: true,
                    required: true
                },
                chatId: {
                    type: String,
                    unique: true,
                    // default: ''
                },
            },
            { timestamps: true }
        )
    );
};
