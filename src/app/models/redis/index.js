import redis from 'redis';

export default {
    async connect() {

        this.client = await redis.createClient({
            port: CONFIGS.REDIS_PORT,
            host: CONFIGS.REDIS_HOST,
        });
        this.client.on('error', (error) => {
            console.log(`Error REDIS ${error}`);
        });
        this.client.on('connect', () => {
            console.log('REDIS connection ready');
        });
        this.client.select(CONFIGS.REDIS_DB);

    },

    getter(key) {
        return new Promise((resolve, reject) => {
            this.client.get(key, (error, response) => {
                if (error) reject(error);
                resolve(JSON.parse(response));
            });
        });
    },

    setter(key, value) {
        return new Promise((resolve, reject) => {
            this.client.set(key, JSON.stringify(value), (error, response) => {
                if (error) reject(error);
                resolve(response);
            });
        });
    },

    deleter(key) {
        return new Promise((resolve, reject) => {
            this.client.del(key, (error, response) => {
                if (error) reject(error);
                resolve(response);
            });
        });
    },

    keys(key) {
        return new Promise((resolve, reject) => {
            this.client.keys(key, (error, response) => {
                if (error) reject(error);
                resolve(response);
            });
        });
    },
};
