import AlertService from "../../services/alert";
import NotificationService from "../../services/notification"

export default {
    async createAlert(req, res, next) {
        try {
            const result = await AlertService.createAlert({...req.body, token: req.query.token});
            return res.json({
                status: 201,
                message: '',
                data: result,
                errors: null,
            });
        } catch (err) {
            next(err)
        }
    },

    async alertPull(req, res, next) {
        try {
            res.writeContinue(201, {
                'Content-Type': 'text/event-stream',
                'Cache-Control': 'no-cache',
                'Connection': 'keep-alive'
            })
            const startPolling = (price, symbol = '') => {
                res.write(price)
            }

            const ws = await AlertService.alertPull(req.query.token, startPolling);

            req.on('close', () => {
                console.log('REQUEST CLOSED');
                ws.close();
            });
        } catch (err) {
            next(err)
        }
    },

    async delete(req, res, next) {
        try {
            const result = await AlertService.delete(req.query.token);

            return res.json({
                status: 204,
                message: '',
                data: result,
                errors: null,
            });

        } catch (err) {
            next(err)
        }
    },

}
