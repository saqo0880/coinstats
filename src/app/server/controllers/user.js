import UserService from "../../services/user";

export default {
    async init(req, res, next) {
        try {
            const result = await UserService.init(req.query);
            return res.json({
                status: 201,
                message: '',
                data: result,
                errors: null,
            });
        } catch (err) {
            next(err)
        }
    },

    async getTrades(req, res, next) {
        try {
            const result = await UserService.getTrades(req.query.token)
            return res.json({
                status: 200,
                message: '',
                data: result,
                errors: null,
            });
        } catch (err) {
            next(err)
        }
    },

    async balance(req, res, next) {
        try {
            const result = await UserService.getBalance(req.query.token);

            return res.json({
                status: 201,
                message: '',
                data: result,
                errors: null,
            });

        } catch (err) {
            next(err)
        }
    },

}
