import express from 'express';
import * as http from 'http';
import cors from 'cors';
import morgan from 'morgan';
import routes from './routes';
import {ErrorHandler} from "../../helpers/errorHandler";

export default {
    init() {
        this.createApp();
        this.createServer();
        return this.setupExpress();
    },

    createApp() {
        this.app = express();

    },

    createServer() {
        this.server = http.createServer(this.app);
    },

    setupExpress() {
        this.app.disable('x-powered-by');
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended: false}));
        this.app.use(morgan('tiny'));
        this.app.use(cors());
        this.app.use('/api', routes);

        this.app.use((req, res) => {
            res.status(404).json({
                status: 'Error',
                message: '',
                data: null,
                errors: '',
            });
        });
        this.app.use((error, req, res, next) => {
            if (error instanceof ErrorHandler) {
                return res.status(error.status || 400).json({
                    status: error.name,
                    message: error.message,
                    data: null,
                    errors: error.errors,
                });
            }
            return res.status(500).json({
                status: 'Error',
                message: error.message || error.msg,
                data: null,
                errors: null,
            });
        });

        return new Promise((res) => {
            this.server.listen(CONFIGS.API_PORT, () => {
                console.log('API started ', CONFIGS.API_PORT);
                res();
            });
        });
    },
};
