import RedisModel from "../../models/redis";
import {AuthError} from "../../../helpers/errorHandler";

export default async (token) => {
    try {
        const user = await RedisModel.getter(`token-${token}`);
        if (!user) {
            throw new AuthError()
        }
        return true;
    } catch (err) {
        throw err;
    }
}