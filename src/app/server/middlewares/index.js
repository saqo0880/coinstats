import RedisModel from '../../models/redis';
import {AuthError, ValidationError} from "../../../helpers/errorHandler";

exports.init = async (req, res, next) => {
    try {
        const {key, secret} = req.query;
        if (!key && !secret) {
            next(new ValidationError())
            return;
        }
        next();
    } catch (err) {
        next(err)
    }

};

exports.auth = async (req, res, next) => {
    try {
        const token = req.query.token;

        console.log('TOKEN => ', token)
        if (!token) {
            next(new ValidationError())
            return;
        }
        const user = await RedisModel.getter(`token-${token}`);
        if (!user) {
            next(new AuthError())
        }
        next();
    } catch (err) {
        next(err)
    }

};
