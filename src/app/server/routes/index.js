import express from 'express';
import UserController from "../controllers/user";
import AlertController from "../controllers/alert";
import {auth, init} from '../middlewares';

const router = express.Router();

//-------------------- Users routes --------------------

router.get('/init', init, UserController.init);

router.get('/getAllTradingPairs', auth, UserController.getTrades);

router.get('/getUserBalance', auth, UserController.balance);


//-------------------- Alerts routes --------------------

router.get('/alertPull', AlertController.alertPull);

router.post('/createAlert', auth, AlertController.createAlert);

router.delete('/destroy', auth, AlertController.delete);


export default router;
