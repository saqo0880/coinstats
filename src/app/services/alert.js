import BinanceService from './binance';
import RedisModel from '../models/redis';
import {AuthError} from "../../helpers/errorHandler";
import NotificationService from "./notification";

export default {
    async createAlert({token, min, max, symbol}) {
        try {

            const User = DB.users;
            const Alert = DB.alerts;
            const user = await User.findOne({_id: token});

            if (!user) {
                throw new AuthError()
            }

            const {chatId, key, secret} = user;

            if (min > max) {
                [min, max] = [max, min]
            }

            await BinanceService.tradeRest(key, secret, symbol);

            const alert = new Alert({
                min,
                max,
                userId: token,
                symbol
            })

            const {_id: alertToken} = await alert.save();
            let symbolData = await RedisModel.getter(`symbol-${symbol}`);

            if (!symbolData) {
                symbolData = {
                    min: [{userId: token, chatId, alertToken, price: min}],
                    max: [{userId: token, chatId, alertToken, price: max}]
                }
            } else {
                symbolData.min.push({userId: token, chatId, alertToken, price: min});
                symbolData.max.push({userId: token, chatId, alertToken, price: max});
            }

            await RedisModel.setter(`symbol-${symbol}`, symbolData);

            NotificationService.sendNotification(CONFIGS.NOTIFICATIONS.ADD_NEW_SYMBOL, {symbol, symbolData})

            return {alertToken};

        } catch (err) {
            console.error('AlertService createAlert ERROR => ', err)

            if (err.response) {
                throw new Error(err.response.data.msg)
            }
            throw err;
        }
    },

    async alertPull(token,cb) {
        try {
            const Alert = DB.alerts;

            const alert = await Alert.findOne({_id: token, isActive: true});

            if (!alert) {
                throw new Error('Alert doesnt exist')
            }
            const {symbol} = alert;

            return BinanceService.tradeWS(symbol,cb)

        } catch (err) {
            console.error('AlertService alertPull ERROR => ', err)
            if (err.response) {
                throw new Error(err.response.data.msg)
            }
            throw err;
        }

    },

    async delete(token) {
        try {

            const Alert = DB.alerts;

            const alerts = await Alert.find({userId: token, isActive: true})

            for (let i = 0; i < alerts.length; i += 1) {
                let symbol = alerts[i].symbol;
                let alertToken = alerts[i]._id
                // await RedisModel.deleter(`symbol-${symbol}`);

                // NotificationService.removeSymbol({userId: token, symbol});

                NotificationService.sendNotification(CONFIGS.NOTIFICATIONS.REMOVE_SYMBOL, {userId: token, alertToken, symbol})
            }

            await Alert.updateMany({userId: token}, {"$set": {"isActive": false}});

            return {}
        } catch (err) {
            console.error('UserService delete ERROR => ', err)
            throw err;
        }

    },

}
