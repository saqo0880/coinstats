import WebSocket from 'ws';
import axios from 'axios';
import createSignature from "../../helpers/createSignature";
import NotificationService from "./notification"


export default {
    account(key, secret) {
        try {
            const timestamp = `timestamp=${Date.now()}`;

            const signature = createSignature(timestamp, secret);

            return axios.get(`${CONFIGS.REST_URL}/v3/account?recvWindow=60000&${timestamp}&signature=${signature}`, {
                headers: {
                    'X-MBX-APIKEY': key,
                }
            });

        } catch (err) {
            throw new Error('Invalid Symbol');
        }

    },

    exchangeInfo(key) {
        try {
            return axios.get(`${CONFIGS.REST_URL}/v3/exchangeInfo`, {
                headers: {
                    'X-MBX-APIKEY': key,
                }
            });

        } catch (err) {
            throw new Error('Invalid Symbol');
        }
    },

    async tradeRest(key, secret, symbol) {
        try {
            await this.account(key, secret);
            return axios.get(`${CONFIGS.REST_URL}/v1/trades?symbol=${symbol}`, {
                headers: {
                    'X-MBX-APIKEY': key,
                }
            });
        } catch (error) {
            throw new Error('Invalid account key & secret');
        }

    },

    tradeWS(symbol, cb) {
        const ws = new WebSocket(`${CONFIGS.WS_URL}${symbol.toLowerCase()}@trade`, {
            perMessageDeflate: false
        });
        try {
            let event;
            ws.on('message', (message) => {

                try {
                    event = JSON.parse(message);
                } catch (e) {
                    event = message;
                }

                cb(event.p, symbol);
                // NotificationService.sendNotification(CONFIGS.NOTIFICATIONS.UPDATE_PRICE, {
                //     price: event.p,
                //     symbol,
                //     chatId
                // })
            })

            ws.on('error', (err) => {
                console.error('WS ERROR ', err)

                ws.close()
                // node.js EventEmitters will throw and then exit if no error listener is registered
            });
            return ws;
        } catch (err) {
            ws.close();
            throw new Error(err);
        }
    },

}
