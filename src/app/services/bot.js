import {Telegraf} from "telegraf";
import UserService from '../services/user'
import authBot from "../server/middlewares/authBot";
import AlertService from "./alert";
import NotificationService from "./notification"

export default {
    init() {
        this.connect();
    },
    connect() {
        this.bot = new Telegraf(CONFIGS.BOT_KEY);

        console.log('Bot ready')

        this.bot.start((ctx) => ctx.reply('Welcome'));

        this.bot.command('/init', this.start.bind(this));
        this.bot.command('/getAllTradingPairs', this.getAllTradingPairs.bind(this));
        this.bot.command('/getUserBalance', this.getUserBalance.bind(this));
        this.bot.command('/destroy', this.destroy.bind(this));

        this.bot.command('/createAlert', this.createAlert.bind(this))
        this.bot.command('/alertPull', this.alertPull.bind(this))


        NotificationService.on(CONFIGS.NOTIFICATIONS.SEND_BOT_MESSAGE, (data) => {
            this.sendMessage(data)
        });


        this.bot.launch();
    },

    async start(ctx) {
        try {
            const [, key, secret] = ctx.update.message.text.split(' ');

            const chatId = ctx.message.chat.id

            let result;
            if (key && secret) {
                result = await UserService.init({key, secret, chatId})
                ctx.reply(`Token: ${result.token}`);
                return
            }
            ctx.reply(new Error('Validation error'));

        } catch (err) {
            ctx.reply(err.message);
        }
    },

    async getUserBalance(ctx) {
        try {
            const [, token] = ctx.update.message.text.split(' ');

            const isLogin = await authBot(token);

            if (isLogin) {
                const result = await UserService.getBalance(token);

                ctx.reply(`Balance: ${result.balances.length || 0}`);
                return
            }
            ctx.reply(new Error('Auth error'));

        } catch (err) {
            ctx.reply(err.message);
        }
    },

    async getAllTradingPairs(ctx) {
        try {
            const [, token] = ctx.update.message.text.split(' ');

            const isLogin = await authBot(token);

            if (isLogin) {
                const result = await UserService.getTrades(token, true);

                ctx.replyWithDocument({source: result, filename: 'test.csv'});
                return;
            }
            ctx.reply(new Error('Auth error'));


        } catch (err) {
            return ctx.reply(err.message);
        }
    },

    async destroy(ctx) {
        try {
            const [, token] = ctx.update.message.text.split(' ');

            const isLogin = await authBot(token);

            if (isLogin) {
                const result = await AlertService.delete(token);

                ctx.reply(result);
                return
            }
            ctx.reply(new Error('Auth error'));

        } catch (err) {
            ctx.reply(err.message);
        }
    },

    async createAlert(ctx) {
        try {
            const [, token, symbol, min, max] = ctx.update.message.text.split(' ');

            const isLogin = await authBot(token);

            if (isLogin) {
                const result = await AlertService.createAlert({token, symbol, min, max});

                ctx.reply(result);
                return
            }
            ctx.reply(new Error('Auth error'));

        } catch (err) {
            return ctx.reply(err.message);
        }
    },

    async alertPull(ctx) {
        try {
            const [, token] = ctx.update.message.text.split(' ');

            const startPolling = (price,symbol) => {
                ctx.reply(`${symbol} -> ${price}`);
            }

            const ws = await AlertService.alertPull(token, startPolling);

            // setTimeout(() => {
            //     ws.close()
            // }, 2000)

        } catch (err) {
            return ctx.reply(err.message);
        }
    },

    sendMessage(data) {
        try {
            const {chatId, msg} = data;

            if (!chatId) {
                return
            }

            this.bot.telegram.sendMessage(chatId, msg)
        } catch (err) {

            console.log('sendMessage ERR=> ', err)

        }

    }

}
