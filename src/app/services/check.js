import RedisModel from "../models/redis";
import BinanceService from "./binance"
import NotificationService from './notification';

export default {
    map: {},
    sockets: {},
    currentPrices: {},

    async init() {
        this.setupListeners();
        await this.recoverMap()
    },

    setupListeners() {
        NotificationService.on(CONFIGS.NOTIFICATIONS.ADD_NEW_SYMBOL, (data) => {
            this.addNewSymbol(data)
        });
        NotificationService.on(CONFIGS.NOTIFICATIONS.REMOVE_SYMBOL, (data) => {
            this.removeSymbol(data)
        });

        // NotificationService.on(CONFIGS.NOTIFICATIONS.UPDATE_PRICE, (data) => {
        //     this.changeCurrentPrices(data)
        // })
    },

    async recoverMap() {
        try {
            const redisKeys = await RedisModel.keys('symbol-*')

            for (let key of redisKeys) {
                const data = await RedisModel.getter(key);
                const symbol = key.replace('symbol-', '');

                this.map[symbol] = data;
            }
            this.startSubscribing();

        } catch (err) {
            console.error('Notifications service error=> ', err)
        }
    },

    startSubscribing() {
        Object.keys(this.map).forEach(symbol => {
            // if (this.map[symbol].min.length || this.map[symbol].max.length) {
            this.sockets[symbol] = BinanceService.tradeWS(symbol, this.changeCurrentPrices.bind(this))
            // }
        })
        this.interval();
    },

    changeCurrentPrices(price, symbol) {
        this.currentPrices[symbol] = price;
    },

    interval() {
        setTimeout(() => {
            Object.keys(this.currentPrices).forEach(symbol => {
                // console.log('CURRENT PRICE => ', this.currentPrices[symbol])
                this.map[symbol].min.forEach(el => {

                    if (this.currentPrices[symbol] <= Number(el.price)) {

                        console.log('MIN => ', Number(el.price))

                        // this.sendNotification(el, symbol, this.currentPrices[symbol], 'MIN');
                        // NotificationService.emit('sendChatMessage', {
                        //     user: el,
                        //     symbol,
                        //     price: this.currentPrices[symbol],
                        //     what: 'MIN'
                        // })

                        NotificationService.emit(CONFIGS.NOTIFICATIONS.SEND_BOT_MESSAGE, {
                            chatId: el.chatId,
                            data: `Alert ${symbol}  with ${el.alertToken} id reached his MIN limits${this.currentPrices[symbol]}`
                        })

                        this.removeSymbol({userId: el.userId, symbol, alertToken: el.alertToken})
                        console.log('------------------------ MIN ------------------------')
                    }
                })
                this.map[symbol].max.forEach(el => {
                    if (this.currentPrices[symbol] > Number(el.price)) {
                        console.log('MAX => ', Number(el.price))

                        // this.sendNotification(el, symbol, this.currentPrices[symbol], 'MAX');

                        this.removeSymbol({userId: el.userId, symbol, alertToken: el.alertToken})

                        NotificationService.emit(CONFIGS.NOTIFICATIONS.SEND_BOT_MESSAGE, {
                            chatId: el.chatId,
                            msg: `Alert ${symbol}  with ${el.alertToken} id reached his MAX limits${this.currentPrices[symbol]}`
                        })
                        console.log('------------------------ MAX ------------------------')
                    }
                })
            })
            this.interval()
        }, 2000)
    },

    closeAllConnections() {
        Object.values(this.sockets).forEach(ws => {
            console.log('------ closeAllConnections ------');
            ws.close()
        });
        this.sockets = {};
    },

    removeSymbol({userId, alertToken, symbol}) {
        try {

            this.map[symbol].min = this.map[symbol].min.filter(data => String(data.alertToken) !== String(alertToken));
            this.map[symbol].max = this.map[symbol].max.filter(data => String(data.alertToken) !== String(alertToken));

            RedisModel.setter(`symbol-${symbol}`, this.map[symbol])

            if (!this.map[symbol].min.length || !this.map[symbol].max.length) {
                console.log('CLOSE CONNECTION ', symbol);
                RedisModel.deleter(`symbol-${symbol}`)
                this.sockets[symbol].close();
            }
        } catch (err) {
            throw new Error(err)
        }
    },

    addNewSymbol({symbol, symbolData, isNew}) {

        this.map[symbol] = {...symbolData};

        // this.map[symbol].min.push(symbolData.min)
        // this.map[symbol].max.push(symbolData.max)

        this.closeAllConnections();
        this.startSubscribing();
    },
}