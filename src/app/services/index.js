import BotService from './bot';
import CheckService from './check';

export default {
    async init() {
        BotService.init()
        await CheckService.init()
    },
};
