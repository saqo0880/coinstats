import BinanceService from './binance';
import RedisModel from '../models/redis';
import {AuthError} from "../../helpers/errorHandler";

export default {
    async init({key, secret, chatId}) {
        try {
            await BinanceService.account(key, secret);

            const User = DB.users;

            const query = {key, secret};
            const update = {$set: {key, secret, chatId}};
            const options = {upsert: true, new: true};
            const {_id: token} = await User.findOneAndUpdate(query, update, options)

            await RedisModel.setter(`token-${token.toString()}`, {key, secret, chatId});

            return {token};

        } catch (err) {
            console.error('UserService Init ERROR => ', err);
            if (err.response) {
                throw new Error(err.response.data.msg)
            }
            throw err;
        }
    },

    async getTrades(token, isForBot = false) {
        try {
            const User = DB.users;

            const user = await User.findOne({_id: token});

            if (!user) {
                throw new AuthError()
            }

            const response = await BinanceService.exchangeInfo(token);

            return isForBot ? Buffer.from(response.data.symbols.map((i) => i.symbol).join('\n')) : response.data.symbols;
        } catch (err) {
            console.error('UserService getALl ERROR => ', err)
            if (err.response) {
                throw new Error(err.response.data.msg)
            }
            throw err;
        }
    },

    async getBalance(token) {
        try {
            const User = DB.users;
            const user = await User.findOne({_id: token});

            if (!user) {
                throw new AuthError()
            }

            const {key, secret} = user;
            const result = await BinanceService.account(key, secret);

            return {balances: result.data.balances};

        } catch (err) {
            console.error('UserService getBalance ERROR => ', err)
            if (err.response) {
                throw new Error(err.response.data.msg)
            }
            throw err;
        }

    },

}
