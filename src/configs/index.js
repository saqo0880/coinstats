export default {
    API_PORT: process.env.API_PORT,
    MONGO: {
        user: process.env.MONGO_USER,
        password: process.env.MONGO_PASS,
        database: process.env.MONGO_DB,
    },
    NOTIFICATIONS : {
        ADD_NEW_SYMBOL : 'addNewSymbol',
        REMOVE_SYMBOL : 'removeSymbol',
        UPDATE_PRICE : 'updatePrice',

        SEND_BOT_MESSAGE : 'sendBotMessage'
    },
    REDIS_DB: parseInt(process.env.REDIS_DB, 10),
    REDIS_PORT: process.env.REDIS_PORT,
    REDIS_HOST: process.env.REDIS_HOST,
    BOT_KEY: process.env.BOT_KEY,
    WS_URL: process.env.WS_URL,
    REST_URL: process.env.REST_URL
};
