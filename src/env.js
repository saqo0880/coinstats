import dotenv from 'dotenv';

dotenv.config({ path: `${process.cwd()}/.env` });
global.CONFIGS = require('./configs').default;
