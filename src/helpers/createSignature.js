import crypto from 'crypto';

export default function (timestamp, secret) {
    return crypto
        .createHmac('sha256', secret)
        .update(`recvWindow=60000&${timestamp}`)
        .digest('hex');
}