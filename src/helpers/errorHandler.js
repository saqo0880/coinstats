export class ErrorHandler extends Error {
    constructor() {
        super();
    }
}

export class NotFound extends ErrorHandler {
    constructor(
        message = 'NotFound Error',
        errors = null,
        name = 'NOT_FOUND',
        status = 404,
    ) {
        super();
        this.data = null;
        this.message = message;
        this.errors = errors;
        this.status = status;
        this.name = name;
    }
}

export class AuthError extends ErrorHandler {
    constructor(
        message = 'Auth Error',
        errors = null,
        name = 'UNAUTHORIZED',
        status = 401,
    ) {
        super();
        this.data = null;
        this.message = message;
        this.errors = errors;
        this.status = status;
        this.name = name;
    }
}

export class ValidationError extends ErrorHandler {
    constructor(
        message = 'Validation Error',
        errors = null,
        name = 'VALIDATION',
        status = 422,
    ) {
        super();
        this.data = null;
        this.message = message;
        this.errors = errors;
        this.status = status;
        this.name = name;
    }
}
